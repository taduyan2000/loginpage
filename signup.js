var userkey = 'user';
var dataString = localStorage.getItem(userkey);

var user;

if (dataString) {
    user = JSON.parse(dataString)
} else {
    user = [];
}

function addlist() {
    var input = document.getElementById('myinput');
    var newItem = input.value;

    user.push(newItem);

    render();

    input.value = " ";

    localStorage.setItem(userkey, JSON.stringify(user));
}