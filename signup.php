<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="signup.css">
    <script src="magic.js"> </script>
    <title>Sign up</title>
</head>

<body>
    <div class="content-wrapper">
        <div class="content">
            <div class="signup-wrapper shadow-box">

                <div class="company-details">
                    <div class="shadow"></div>
                    <div class="wrapper-1">
                        <div class="logo">
                            <div class="icon-food">

                            </div>
                        </div>
                        <h1 class="title"></h1>
                        <div class="slogan">We deliver meow meow to you.</div>
                        
                        <a href="loginpage.php" class="link"><button type="submit" class="login" style="margin-left: 8rem; margin-top: 1.2rem; cursor: pointer">Login</button></a>
                    </div>
                </div>

                <div class="signup-form">
                    <div class="wrapper-2">
                        <div class="form-title">Sign up today!</div>
                        <div class="form">
                            <form onsubmit="signup()">
                                <p class="content-item">
                                    <label>email address
                                        <input id="email" type="text" placeholder="taduyan2000@gmail.com" name="email" required>
                                    </label>
                                </p>

                                <p class="content-item">

                                    <label>password
                                        <input type="password" id="password" placeholder="*****" name="psw" required>
                                    </label>
                                </p>

                                <button type="submit" style="margin-left: 5rem; cursor: pointer;" class="signup">Sign up</button>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

</body>

</html>