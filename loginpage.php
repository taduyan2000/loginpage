<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="login.css">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Itim|Lobster|Montserrat:500|Noto+Serif|Nunito|Patrick+Hand|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto+Slab|Saira" rel="stylesheet">
    <script src="magic.js"></script>

    <title>Login</title>


</head>

<body>

    <div class="bg"></div>
    <div class="bg bg2"></div>
    <div class="bg bg3"></div>

    <div class="contentner">
        <h1 style="font-weight: 900; text-align: right; margin-right: 90px; font-family: 'Montserrat', sans-serif;">Sign
            in</h1>

        <div class="form-field" style="margin-top:30px; float:left">
            <input type="text" id="email" class="form-input txtuser" placeholder=" " autocomplete="off"></input>
            <label class="label-input" for="account">Email</label>
        </div>

        <div class="form-field" style="float:left; margin-top: 15px">
            <input type="password" id="password" class="form-input txtpass" placeholder=" " autocomplete="off"></input>
            <label class="label-input" id="password" for="password">Password</label>
        </div>

        <div class="form-field-1">
            <h1 style="color: white; font-weight: 900;font-family: 'Montserrat', sans-serif; margin-top: 4rem;">Hello</h1>
            <p style="color: white ;font-family: 'Montserrat', sans-serif; margin-top: 2rem;">Đăng kí để trải nghiệm!</p>
            <a href="signup.php"><button type="submit" style="width:150px; float: right;margin-right: 1rem; margin-top: 2rem; cursor: pointer;">Sign up</button></a>
        </div>

        <a href="" class="fogot-pw" style="font-size: 12px; margin-left: 34rem; margin-top:16px; color: blue; font-family: 'Montserrat', sans-serif; text-decoration:solid;">Fogot
            password?</a>
        <button type="submit" onclick="login()" class="submit px-4" style="cursor: pointer;">Login</button>
    </div>

</body>

</html>